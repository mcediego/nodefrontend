$(document).ready(function () {

  //uxAccordion.init();
  uxAccordion.selector = '.cmp-modules';
  uxAccordion.faqBindAccordion();
})


var uxAccordion = {

  selector: '.faq',

  init: function () {

    this.faqBindAccordion();
    this.selector = '.cmp-glossary';
    this.faqBindAccordion();
    this.selector = '.cmp-modules';
    this.faqBindAccordion();
  },

  faqBindAccordion: function () {

    var SELF = this;

    $(this.selector+' .title').each(function (index) {

      //Title Click
      $(this).click(function () {

        var icon = $(this).find('.icon');
        var text  = $(this).next().find('.text');

        //Remove preivous states
        $(".faq-open").each(function () {
          $(this).removeClass('faq-open');
        });

        if(icon.hasClass('fa-chevron-down')) {
          SELF.faqOpen(icon,text);
          $(this).addClass('faq-open');
        }
        else {
          SELF.faqClose(icon,text);
        }

        SELF.faqResetAccordion();

      });

    });
  },

  faqClose: function (icon,text) {
    icon.removeClass('fa-chevron-up');
    icon.addClass('fa-chevron-down');
    text.slideUp('slow');
  },

  faqOpen: function (icon,text) {
    icon.removeClass('fa-chevron-down');
    icon.addClass('fa-chevron-up');
    text.slideDown('slow');
  },

  faqResetAccordion: function () {

    var SELF  = this;
    var texts = $(this.selector + ' .text');

    texts.each(function (index) {
      var title = $(this).parent().parent().prev();
      var icon  = $(this).parent().parent().prev().find('.icon');
      if(title.hasClass('faq-open')!=true) {

        SELF.faqClose(icon,$(this));
      }
    });

  },

}
