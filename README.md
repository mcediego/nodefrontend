# Contenuto
Il repo contiene un server http su nodejs con i seguenti tool frontend già disponibili:
- Bootstrap
- jQuery
- Fontawesome

**Alte info**<br>
Il server risponde all'indirizzo http://localhost:3000/ <br>
le pagine sono disponibili nella cartella www/ <br>
All'interno è presente un esempio di layout e di accordion da riutilizzare per il progetto

# Installazione
```
git clone https://mcediego@bitbucket.org/mcediego/nodefrontend.git
cd cabelfe
npm install
node server.js
```
